JUEGO DEL WORDLE. por Asier Barranco

El objetivo de este proyecto truncal (correspondiente a M3_PT1) es hacer el juego del Wordle.

El Wordle es un juego que consiste a adivinar una palabra de 5 letras en 6 intentos. Cada vez que introduces una palabra el juego revisa las letras y comprueba si están
incluidas en la palabra. Las letras que estén en la posición exacta las mostrará con fondo verde. Las que no estén en posición exacta pero sí incluidas en la palabra las
mostrará con fondo amarillo. Las letras que no estén en la palabra las mostrará con fondo gris.

COSAS IMPLEMENTADAS EN EL PROYECTO:

-Un diccionario con alrededor de 5000 palabras de 5 letras
-Visualización diferenciada por pantalla dependiendo de las letras contenidas en la palabra seleccionada
-Un control sobre las palabras introducidas y su veracidad o su longitud

POSIBLES MEJORAS / DETECCIÓN DE ERRORES:

Implementar más efectos visuales al inicio del programa. Como las letras de WORDLE de colores diferentes, con fondos degradados y ASCII art.
No las he implementado por falta de tiempo y conocimiento sobre esto último.

He encontrado un error del cual no tengo ni idea como solucionar y es que si una letra aparece una sola vez en la palabra acertada pero más de una en la palabra introducida por
el usuario ésta se verá marcada 2 veces por pantalla. Un error al que le he dado mil vueltas y no he conseguido corregir

QUÉ HE APRENDIDO:

En este proyecto he aprendido a cómo trabajar y personalizar la interfaz/terminal de IntelliJ con Kotlin. Cambiando los colores del fondo, de las letras etc...
También he refrescado conocimientos tales como el uso de Arrays y la comparación de Strings, así como el uso de bucles.